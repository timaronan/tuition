app.controller('schoolCtrl', function ($scope, $state, Data) {
    $scope.majors = [{}];
    $scope.regions = [{}];
    $scope.schools = [{}];
    $scope.currentSalary = {
        'title' : 'Average Starting',
        'val' : 'start_sal'
    }
    $scope.salaryRange = "Average";
    $scope.careerPosition = "Starting";
    $scope.yearsEnrolled = 4;
    $scope.randomQuote = "I hope you like top ramen";
    $scope.selectRate = {'interest': 5,'term': 10};
    $scope.family = {'adults': 1,'kids': 0};
    $scope.initExpenses = ['food','housing','transport','health'];
    $scope.currentExpenses = {
        'housing' : 0,
        'food' : 0,
        'child' : 0,
        'transport' : 0,
        'health' : 0,
        'other' : 0
    }
    $scope.careers = [{ 'title' : 'Average Starting', val : 'start_sal'},
                      { 'title' : 'Average Midrage', val : 'mid_sal'},
                      { 'title' : 'Lower 10% Midrage', val : 'mid10_sal'},
                      { 'title' : 'Lower 25% Midrage', val : 'mid25_sal'},
                      { 'title' : 'Upper 75% Midrage', val : 'mid75_sal'},
                      { 'title' : 'Upper 90% Midrage', val : 'mid90_sal'}];

    $scope.expenseUpdate;
    $scope.interestRates = [
        {'loan' : 'subsidized', 'borrower' : 'undergrad', 'rate' : 4.29},
        {'loan' : 'unsubsidized', 'borrower' : 'undergrad', 'rate' : 4.29},
        {'loan' : 'unsubsidized', 'borrower' : 'graduate or professional', 'rate' : 5.84},
        {'loan' : 'plus', 'borrower' : 'parent, graduate, or professional', 'rate' : 6.84}
    ];

    $scope.selectSchool, $scope.selectMajor, $scope.selectArea, $scope.selectPayments,
        $scope.totalMonthly, $scope.totalLeft, $scope.federalTaxes, $scope.stateTaxes,
        $scope.taxDisplay, $scope.expenses, $scope.premiums;
    
    $scope.livingExpenses = 0;
    $scope.currentTaxes = {'state':0,'federal':0};
    $scope.selectSchoolObj, $scope.selectMajorObj;

    Data.get('schools').then(function(data){
        $scope.schools = data.data;
        // $('.loading.loader').addClass('fadeOut');
        // setTimeout(function(){
        //     $state.go('.school');
        // }, 100);
    });
    Data.get('majors').then(function(data){
        $scope.majors = data.data;
    });
    Data.get('premiums').then(function(data){
        $scope.premiums = data.data;
    });
    Data.get('vareas').then(function(data){
        $scope.regions = data.data;

        setTimeout(function(){
            combinePremiums();
        }, 5000);

        // little hack so that there aren't errors thrown while calculating selectAreaObj
        Data.get('varea/'+$scope.regions[Math.round(Math.random() * $scope.regions.length)].id).then(function(data){
            $scope.selectAreaObj = data.data;
        });
    });
    Data.get('federal').then(function(data){
        $scope.federalTaxes = data.data;
    });

    function combinePremiums(){
        console.log("called");

        for (var i = 0; i < $scope.regions.length; i++) {
            for (var j = 0; j < $scope.premiums.length; j++) {
                // console.log($scope.premiums[i].location, $scope.regions[i].state);
                if($scope.premiums[j].location === $scope.regions[i].state){
                    console.log("SUCCESS!!!");
                    $scope.regions[i].premium = $scope.premiums[j].premium;
                    Data.put('varea/update/'+$scope.regions[i].id, $scope.regions[i]).then(function (result) {
                        if(result.status != 'error'){
                            console.log("PUT!");
                        }else{
                            alert(result.message);
                        }
                    });    
                }
            };
        };

    }


    $scope.close = function(){
        $('.edit.form').addClass('closed');
    }
    $scope.open = function(){
        $('.edit.form').removeClass('closed');
    }
    $scope.saveComplete = function(sch,lv,mj,yrs,ir,pb,cs){
        $scope.close();
        if(cs){
            $scope.currentSalary = cs;
        }
        if(yrs){
            $scope.yearsEnrolled = yrs;
            loanPayment();
        }
        if(ir){
            $scope.selectRate.interest = ir;
            loanPayment();
        }
        if(pb){
            $scope.selectRate.term = pb;
            loanPayment();
        }
        
        monthlyExpenses();
        
        if(sch){
            console.log(sch);
            Data.get('school/'+sch.id).then(function(data){
                $scope.selectSchoolObj = data.data;
                console.log($scope.selectSchoolObj);

                $scope.totalMonthly = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) * 0.9;
                $scope.totalLeft = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) - $scope.totalMonthly;

                loanPayment();
                monthlyExpenses();
            });             
        }
        if(lv){
            Data.get('varea/'+lv.id).then(function(data){
                $scope.selectAreaObj = data.data;
                stateTaxed($scope.selectAreaObj[0].state);
                monthlyExpenses();
            });
            // Data.get('area/'+lv.id).then(function(data){
            //     $scope.selectAreaObj = data.data;
            //     stateTaxed($scope.selectAreaObj[0].state);
            //     monthlyExpenses();
            // });
        }
        if(mj){
            Data.get('major/'+mj.id).then(function(data){
                $scope.selectMajorObj = data.data;
                fedTaxed();
                monthlyExpenses();
            });
        }
    }
    $scope.closeOverlay = function(){
        $('.living.payment .overlay').addClass('closed');
    }
    $scope.openOverlay = function(){
        $('.living.payment .overlay').removeClass('closed');
        $scope.expenses = angular.copy($scope.currentExpenses);
        console.log($scope.expenses);
    }
    $scope.updateExpense = function(exp){
        $('.living.payment .overlay').addClass('closed');
        if(exp){
            console.log(exp);
            $scope.expenseUpdate = exp;
            $scope.currentExpenses.active = true;
            $scope.initExpenses = [];
            for (var i = Object.keys(exp).length - 1; i >= 0; i--) {
                if(exp[Object.keys(exp)[i]] > 0){
                    $scope.initExpenses.push(Object.keys(exp)[i]);
                }
            };
            monthlyExpenses();
        }
    }
    $scope.goMajors = function(ss){
        if(ss){
            $scope.selectSchool = ss;
            
            Data.get('school/'+ss.id).then(function(data){
                $scope.selectSchoolObj = data.data;
                console.log($scope.selectSchoolObj);
            });

            if($state.current.name === 'tuition'){
                $state.go('.major');
            }else{
                $state.go('^.major');
            }
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goSchools = function(){
        if($state.current.name === 'tuition'){
            $state.go('.school');
        }else{
            $state.go('^.school');
        }
    }
    $scope.goAreas = function(sm){
        if(sm){
            $scope.selectMajor = sm;
            
            Data.get('major/'+sm.id).then(function(data){
                $scope.selectMajorObj = data.data;
                console.log($scope.selectMajorObj);
            });            
            if($state.current.name === 'tuition'){
                $state.go('.area');
            }else{
                $state.go('^.area');
            }        
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goRates = function(sa){
        if(sa){
            $scope.selectArea = sa;
            
            Data.get('area/'+sa.id).then(function(data){
                $scope.selectAreaObj = data.data;
                console.log($scope.selectAreaObj);
            });      
            if($state.current.name === 'tuition'){
                $state.go('.rates');
            }else{
                $state.go('^.rates');
            }
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goRatesDefault = function(){
        if($state.current.name === 'tuition'){
            $state.go('.ratesdefault');
        }else{
            $state.go('^.ratesdefault');
        }
    }
    $scope.goComplete = function(sa){
        if(sa){
            // $scope.selectRate = sr;
            calculate(sa);

            if($state.current.name === 'tuition'){
                $state.go('.complete');
            }else{
                $state.go('^.complete');
            }
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }

    }
    function taxes(val){
        return $scope.selectMajorObj[0][$scope.currentSalary.val] * val;
    }
    function totalTax(){
        $scope.taxDisplay = taxes($scope.currentTaxes.state) + taxes($scope.currentTaxes.federal);
    }
    function monthlyExpenses(){
        if($scope.selectAreaObj){
            getLivingExpenses($scope.initExpenses);
        }
        $scope.totalMonthly = ($scope.taxDisplay / 12) + $scope.selectPayments + $scope.livingExpenses;
        $scope.totalLeft = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) - $scope.totalMonthly;
        console.log('monthlys', $scope.totalLeft, $scope.selectMajorObj[0][$scope.currentSalary.val]);
    }
    function stateTaxed(state){
        Data.get('state/'+state).then(function(state){
            $scope.stateTaxes = state.data;
            console.log(state);
            for (var i = 0; i < $scope.stateTaxes.length; i++) {
                if($scope.stateTaxes[i].income < $scope.selectMajorObj[0][$scope.currentSalary.val]){
                    $scope.currentTaxes.state = (parseFloat($scope.stateTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }
            };
        });
    }
    function fedTaxed(state){
        for (var i = $scope.federalTaxes.length - 1; i >= 0; i--) {
            var sp;
            if($scope.federalTaxes[i].single.indexOf('<') < 0){
                sp = $scope.federalTaxes[i].single.split("-");
                if(parseInt(sp[0]) <= parseInt($scope.selectMajorObj[0][$scope.currentSalary.val]) <= parseInt(sp[1])){
                    $scope.currentTaxes.federal = (parseFloat($scope.federalTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }
            }else{
                sp = parseInt($scope.federalTaxes[i].single.replace('<', ''));
                if(sp[0] <= $scope.selectMajorObj[0][$scope.currentSalary.val]){
                    $scope.currentTaxes.federal = (parseFloat($scope.federalTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }               
            }
        };
    }
    function getLivingExpenses(arr){
        $scope.livingExpenses = 0;
        if($scope.currentExpenses.active){
            for (var i = arr.length - 1; i >= 0; i--) {
                $scope.currentExpenses[arr[i]] = $scope.expenseUpdate[arr[i]];
            };
        }else{
            for (var i = arr.length - 1; i >= 0; i--) {
                $scope.currentExpenses[arr[i]] = $scope.selectAreaObj[0][arr[i]];
            };
        }
        for (var i = arr.length - 1; i >= 0; i--) {
            $scope.livingExpenses += $scope.currentExpenses[arr[i]];
        };
    }
    function loanPayment(){
        var P = $scope.selectSchoolObj[0].tuition * $scope.yearsEnrolled;
        var J = ($scope.selectRate.interest / 100) / 12;
        var N = $scope.selectRate.term * 12;

        $scope.selectPayments = P * ( J / ( 1 - Math.pow( (1 + J), -N ) ) );
    }
    function calculate(sa){
        $scope.selectArea = sa;
        
        // Data.get('area/'+sa.id).then(function(data){
        //     $scope.selectAreaObj = data.data;
        //     stateTaxed($scope.selectAreaObj[0].state);
        //     monthlyExpenses();
        // });
        Data.get('varea/'+sa.id).then(function(data){
            $scope.selectAreaObj = data.data;
            stateTaxed($scope.selectAreaObj[0].state);
            monthlyExpenses();
        });

        fedTaxed();
        monthlyExpenses();        
        loanPayment();
    }
});

app.controller('schoolCtrl', function ($scope, $state, Data) {
    $scope.majors = [{}];
    $scope.regions = [{}];
    $scope.schools = [{}];
    $scope.currentSalary = {
        'title' : 'Average Starting',
        'val' : 'start_sal'
    }
    $scope.salaryRange = "Average";
    $scope.careerPosition = "Starting";
    $scope.yearsEnrolled = 4;
    $scope.randomQuote = "I hope you like top ramen";
    $scope.selectRate = {'interest': 5,'term': 10};
    $scope.family = {'adults': 1,'kids': 0};
    $scope.initExpenses = ['food','housing','transport','premium'];
    $scope.currentExpenses = {
        'housing' : 0,
        'food' : 0,
        'child' : 0,
        'transport' : 0,
        'premium' : 0,
        'other' : 0
    }
    $scope.careers = [{ 'title' : 'Average Starting', val : 'start_sal'},
                      { 'title' : 'Average Midrage', val : 'mid_sal'},
                      { 'title' : 'Lower 10% Midrage', val : 'mid10_sal'},
                      { 'title' : 'Lower 25% Midrage', val : 'mid25_sal'},
                      { 'title' : 'Upper 75% Midrage', val : 'mid75_sal'},
                      { 'title' : 'Upper 90% Midrage', val : 'mid90_sal'}];

    $scope.expenseUpdate;
    $scope.interestRates = [
        {'loan' : 'subsidized', 'borrower' : 'undergrad', 'rate' : 4.29},
        {'loan' : 'unsubsidized', 'borrower' : 'undergrad', 'rate' : 4.29},
        {'loan' : 'unsubsidized', 'borrower' : 'graduate or professional', 'rate' : 5.84},
        {'loan' : 'plus', 'borrower' : 'parent, graduate, or professional', 'rate' : 6.84}
    ];

    $scope.selectSchool, $scope.selectMajor, $scope.selectArea, $scope.selectPayments,
        $scope.totalMonthly, $scope.totalLeft, $scope.federalTaxes, $scope.stateTaxes,
        $scope.taxDisplay, $scope.expenses;
    
    $scope.livingExpenses = 0;
    $scope.currentTaxes = {'state':0,'federal':0};
    $scope.selectSchoolObj, $scope.selectMajorObj;
    $scope.area;

    // Data.get('schools').then(function(data){
    //     $scope.schools = data.data;
    //     $('.loading.loader').addClass('fadeOut');
    //     setTimeout(function(){
    //         $state.go('.school');
    //     }, 100);
    // });
    // Data.get('majors').then(function(data){
    //     $scope.majors = data.data;
    // });
    // Data.get('areas').then(function(data){
    //     $scope.regions = data.data;
    // });
    Data.get('vareas').then(function(data){
        $scope.regions = data.data;

        // Data.get('federal').then(function(data){
        //     $scope.federalTaxes = data.data;
            Data.get('areas').then(function(data){
                $scope.area = data.data;

                makeLiving();


            });
        // });


        // Data.get('varea/'+$scope.regions[Math.round(Math.random() * $scope.regions.length)].id).then(function(data){
        //     $scope.selectAreaObj = data.data;
        // });
    });

    function makeLiving(){
        console.log("start");

        for (var i = 0; i < $scope.regions.length; i++) {
            for (var j = 0; j < $scope.area.length; j++) {
                var a = $scope.regions[i].area.split('--')[0];
                var b = $scope.area[j].area.split('--')[0];
                // var y = a.indexOf('--');
                // var z = b.indexOf('--');
                // a = a.substring(0, y != -1 ? y : a.length);
                // b = b.substring(0, z != -1 ? z : b.length);
                // console.log(a+" "+$scope.regions[i].state , b+" "+$scope.area[i].state);
                if(a+" "+$scope.regions[i].state === b+" "+$scope.area[j].state){
                    // console.log("worked");
                    var liver = {};
                    liver.state = $scope.regions[i].state;
                    liver.area = $scope.regions[i].area;
                    liver.housing = $scope.area[j].housing;
                    liver.transport = $scope.area[j].transport;
                    liver.child = $scope.area[j].childcare;
                    liver.food = $scope.regions[i].food;
                    liver.premium = $scope.regions[i].premium;
                    liver.childEST = $scope.regions[i].child;
                    console.log($scope.area[j], liver);
                    Data.post('expenses', liver).then(function (result) {
                        if(result.status != 'error'){
                            console.log('POST!');
                        }else{
                            alert(result.message);
                        }
                    });
                }
            };
        };


    }


    // Data.get('federal').then(function(data){
    //     $scope.federalTaxes = data.data;
    // });

    $scope.close = function(){
        $('.edit.form').addClass('closed');
    }
    $scope.open = function(){
        $('.edit.form').removeClass('closed');
    }
    $scope.saveComplete = function(sch,lv,mj,yrs,ir,pb,cs){
        $scope.close();
        if(cs){
            $scope.currentSalary = cs;
        }
        if(yrs){
            $scope.yearsEnrolled = yrs;
            loanPayment();
        }
        if(ir){
            $scope.selectRate.interest = ir;
            loanPayment();
        }
        if(pb){
            $scope.selectRate.term = pb;
            loanPayment();
        }
        
        monthlyExpenses();
        
        if(sch){
            console.log(sch);
            Data.get('school/'+sch.id).then(function(data){
                $scope.selectSchoolObj = data.data;
                console.log($scope.selectSchoolObj);

                $scope.totalMonthly = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) * 0.9;
                $scope.totalLeft = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) - $scope.totalMonthly;

                loanPayment();
                monthlyExpenses();
            });             
        }
        if(lv){
            Data.get('varea/'+lv.id).then(function(data){
                $scope.selectAreaObj = data.data;
                stateTaxed($scope.selectAreaObj[0].state);
                monthlyExpenses();
            });
            // Data.get('area/'+lv.id).then(function(data){
            //     $scope.selectAreaObj = data.data;
            //     stateTaxed($scope.selectAreaObj[0].state);
            //     monthlyExpenses();
            // });
        }
        if(mj){
            Data.get('major/'+mj.id).then(function(data){
                $scope.selectMajorObj = data.data;
                fedTaxed();
                monthlyExpenses();
            });
        }
    }
    $scope.closeOverlay = function(){
        $('.living.payment .overlay').addClass('closed');
    }
    $scope.openOverlay = function(){
        $('.living.payment .overlay').removeClass('closed');
        $scope.expenses = angular.copy($scope.currentExpenses);
        console.log($scope.expenses);
    }
    $scope.updateExpense = function(exp){
        $('.living.payment .overlay').addClass('closed');
        if(exp){
            console.log(exp);
            $scope.expenseUpdate = exp;
            $scope.currentExpenses.active = true;
            $scope.initExpenses = [];
            for (var i = Object.keys(exp).length - 1; i >= 0; i--) {
                if(exp[Object.keys(exp)[i]] > 0){
                    $scope.initExpenses.push(Object.keys(exp)[i]);
                }
            };
            monthlyExpenses();
        }
    }
    $scope.goMajors = function(ss){
        if(ss){
            $scope.selectSchool = ss;
            
            Data.get('school/'+ss.id).then(function(data){
                $scope.selectSchoolObj = data.data;
                console.log($scope.selectSchoolObj);
            });

            if($state.current.name === 'tuition'){
                $state.go('.major');
            }else{
                $state.go('^.major');
            }
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goSchools = function(){
        if($state.current.name === 'tuition'){
            $state.go('.school');
        }else{
            $state.go('^.school');
        }
    }
    $scope.goAreas = function(sm){
        if(sm){
            $scope.selectMajor = sm;
            
            Data.get('major/'+sm.id).then(function(data){
                $scope.selectMajorObj = data.data;
                console.log($scope.selectMajorObj);
            });            
            if($state.current.name === 'tuition'){
                $state.go('.area');
            }else{
                $state.go('^.area');
            }        
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goRates = function(sa){
        if(sa){
            $scope.selectArea = sa;
            
            Data.get('area/'+sa.id).then(function(data){
                $scope.selectAreaObj = data.data;
                console.log($scope.selectAreaObj);
            });      
            if($state.current.name === 'tuition'){
                $state.go('.rates');
            }else{
                $state.go('^.rates');
            }
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goRatesDefault = function(){
        if($state.current.name === 'tuition'){
            $state.go('.ratesdefault');
        }else{
            $state.go('^.ratesdefault');
        }
    }
    $scope.goComplete = function(sa){
        if(sa){
            // $scope.selectRate = sr;
            calculate(sa);

            if($state.current.name === 'tuition'){
                $state.go('.complete');
            }else{
                $state.go('^.complete');
            }
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    function taxes(val){
        return $scope.selectMajorObj[0][$scope.currentSalary.val] * val;
    }
    function totalTax(){
        $scope.taxDisplay = taxes($scope.currentTaxes.state) + taxes($scope.currentTaxes.federal);
    }
    function monthlyExpenses(){
        if($scope.selectAreaObj){
            getLivingExpenses($scope.initExpenses);
        }
        $scope.totalMonthly = ($scope.taxDisplay / 12) + $scope.selectPayments + $scope.livingExpenses;
        $scope.totalLeft = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) - $scope.totalMonthly;
        console.log('monthlys', $scope.totalLeft, $scope.selectMajorObj[0][$scope.currentSalary.val]);
    }
    function stateTaxed(state){
        Data.get('state/'+state).then(function(state){
            $scope.stateTaxes = state.data;
            console.log(state);
            for (var i = 0; i < $scope.stateTaxes.length; i++) {
                if($scope.stateTaxes[i].income < $scope.selectMajorObj[0][$scope.currentSalary.val]){
                    $scope.currentTaxes.state = (parseFloat($scope.stateTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }
            };
        });
    }
    function fedTaxed(state){
        for (var i = $scope.federalTaxes.length - 1; i >= 0; i--) {
            var sp;
            if($scope.federalTaxes[i].single.indexOf('<') < 0){
                sp = $scope.federalTaxes[i].single.split("-");
                if(parseInt(sp[0]) <= parseInt($scope.selectMajorObj[0][$scope.currentSalary.val]) <= parseInt(sp[1])){
                    $scope.currentTaxes.federal = (parseFloat($scope.federalTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }
            }else{
                sp = parseInt($scope.federalTaxes[i].single.replace('<', ''));
                if(sp[0] <= $scope.selectMajorObj[0][$scope.currentSalary.val]){
                    $scope.currentTaxes.federal = (parseFloat($scope.federalTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }               
            }
        };
    }
    function getLivingExpenses(arr){
        $scope.livingExpenses = 0;
        if($scope.currentExpenses.active){
            for (var i = arr.length - 1; i >= 0; i--) {
                $scope.currentExpenses[arr[i]] = $scope.expenseUpdate[arr[i]];
            };
        }else{
            for (var i = arr.length - 1; i >= 0; i--) {
                $scope.currentExpenses[arr[i]] = $scope.selectAreaObj[0][arr[i]];
            };
        }
        for (var i = arr.length - 1; i >= 0; i--) {
            $scope.livingExpenses += $scope.currentExpenses[arr[i]];
        };
    }
    function loanPayment(){
        var P = $scope.selectSchoolObj[0].tuition * $scope.yearsEnrolled;
        var J = ($scope.selectRate.interest / 100) / 12;
        var N = $scope.selectRate.term * 12;

        $scope.selectPayments = P * ( J / ( 1 - Math.pow( (1 + J), -N ) ) );
    }
    function calculate(sa){
        $scope.selectArea = sa;
        
        // Data.get('area/'+sa.id).then(function(data){
        //     $scope.selectAreaObj = data.data;
        //     stateTaxed($scope.selectAreaObj[0].state);
        //     monthlyExpenses();
        // });
        Data.get('varea/'+sa.id).then(function(data){
            $scope.selectAreaObj = data.data;
            stateTaxed($scope.selectAreaObj[0].state);
            monthlyExpenses();
        });

        fedTaxed();
        monthlyExpenses();        
        loanPayment();
    }
});






    Data.get('schools').then(function(data){
        $scope.schools = data.data;
        // $('.loading.loader').addClass('fadeOut');
        // setTimeout(function(){
        //     $state.go('.school');
        // }, 100);
    });
    Data.get('majors').then(function(data){
        $scope.majors = data.data;
    });
    // Data.get('areas').then(function(data){
    //     $scope.regions = data.data;
    // });
    Data.get('expenses').then(function(data){
        $scope.regions = data.data;

        for (var i = 0; i < $scope.regions.length; i++) {
            $scope.regions[i].housing *= 7.69;
            Data.put('expense/update/'+$scope.regions[i].id, $scope.regions[i]).then(function (result) {
                if(result.status != 'error'){
                    console.log('PUT!');
                }else{
                    alert(result.message);
                }
            });
        };

        // console.log($scope.regions);
        // Data.get('expense/'+$scope.regions[Math.round(Math.random() * $scope.regions.length)].id).then(function(data){
        //     $scope.selectAreaObj = data.data;
        // });
    });




    Data.get('expenses').then(function(data){
        $scope.exp = data.data;
        Data.get('areas').then(function(data){
            $scope.regions = data.data;

            for (var i = 0; i < $scope.exp.length; i++) {
                for (var j = 0; j < $scope.regions.length; j++) {
                    var a = $scope.exp[i].area.split('--')[0];
                    var b = $scope.regions[j].area.split('--')[0];
                    if(a+" "+$scope.exp[i].state === b+" "+$scope.regions[j].state){
                        $scope.exp[i].other = $scope.regions[j].other;
                        Data.put('expense/update/'+$scope.exp[i].id, $scope.exp[i]).then(function (result) {
                            if(result.status != 'error'){
                                console.log('PUT!');
                            }else{
                                alert(result.message);
                            }
                        });
                    }
                };
            };

        });
    });

    