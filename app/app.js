var app = angular.module('myApp', ['ui.router', 'ngAnimate', 'angucomplete']);

// app.config(['$locationProvider', function($locationProvider){
//     $locationProvider.html5Mode(true).hashPrefix('!');
// }]);

app.config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider.state("tuition", {
        url: "/start",
        title: "Welcome to Living with Tuition",
        templateUrl: "partials/tuition.html",
        controller: "schoolCtrl"
    }).state("tuition.school", {
        templateUrl: "partials/tuition-school.html"
    }).state("tuition.major", {
        templateUrl: "partials/tuition-majors.html"
    }).state("tuition.area", {
        templateUrl: "partials/tuition-area.html"
    }).state("tuition.rates", {
        templateUrl: "partials/tuition-rates.html"
    }).state("tuition.ratesdefault", {
        templateUrl: "partials/tuition-rates-default.html"
    }).state("tuition.complete", {
        templateUrl: "partials/tuition-complete.html"
    }).state("saved", {
        url: "/saved/:detailId",
        templateUrl: "partials/saved.html",
        controller: "completeCtrl"
    }).state("saved.loaded", {
        templateUrl: "partials/saved-loaded.html"
    }).state('404', {
         templateUrl: 'partials/404.html',
         controller: "nfCtrl"
    }); 
    
    $urlRouterProvider.otherwise("/start");
});

app.run(function ($state,$rootScope) {
  $rootScope.$state = $state;
});

// app.run(function() {
//   FastClick.attach(document.body);
// });