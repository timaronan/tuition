app.controller("ParentCtrl", function ($scope) {
    $scope.homeUrl = 'http://localhost/tuition/';
});

app.controller('schoolCtrl', function ($scope, $state, Data, $controller) {

    $scope.copyUrl = $scope.homeUrl + '/#/saved/' + '50';
    $controller('ParentCtrl', {$scope: $scope});
    $scope.majors = [{}];
    $scope.regions = [{}];
    $scope.schools = [{}];
    $scope.currentSalary = {
        'title' : 'Average Starting',
        'val' : 'start_sal'
    }
    $scope.yearsEnrolled = 4;
    $scope.randomQuote = "I hope you like top ramen";
    $scope.selectRate = {'interest': 4.29,'term': 10};
    $scope.family = {'adults': 1,'kids': 0};
    $scope.initExpenses = ['food','housing','transport','premium','other'];
    $scope.currentExpenses = {
        'housing' : 0,
        'food' : 0,
        'child' : 0,
        'transport' : 0,
        'premium' : 0,
    }
    $scope.careers = [{ 'title' : 'Average Starting', val : 'start_sal'},
                      { 'title' : 'Average Midrage', val : 'mid_sal'},
                      { 'title' : 'Lower 10% Midrage', val : 'mid10_sal'},
                      { 'title' : 'Lower 25% Midrage', val : 'mid25_sal'},
                      { 'title' : 'Upper 75% Midrage', val : 'mid75_sal'},
                      { 'title' : 'Upper 90% Midrage', val : 'mid90_sal'}];

    $scope.expenseUpdate;
    $scope.interestRates = [
        {'loan' : 'subsidized', 'borrower' : 'undergrad', 'rate' : 4.29},
        {'loan' : 'unsubsidized', 'borrower' : 'undergrad', 'rate' : 4.29},
        {'loan' : 'unsubsidized', 'borrower' : 'graduate or professional', 'rate' : 5.84},
        {'loan' : 'plus', 'borrower' : 'parent, graduate, or professional', 'rate' : 6.84}
    ];

    $scope.selectSchool, $scope.selectMajor, $scope.selectArea, $scope.selectPayments,
        $scope.totalMonthly, $scope.totalLeft, $scope.federalTaxes, $scope.stateTaxes,
        $scope.taxDisplay, $scope.expenses, $scope.finishID, $scope.iconClass, $scope.schoolCost;

    $scope.urlCopy = $scope.homeUrl;
    $scope.compensation = 0;
    $scope.livingExpenses = 0;
    $scope.other = 0;
    $scope.currentTaxes = {'state':0,'federal':0};
    $scope.selectSchoolObj, $scope.selectMajorObj, $scope.selectAreaObj;
    $scope.result = "";

    var loadload;
    var neg = ["Is there room in your parent's basement?","How do you feel about permanent camping?","How attached to your kidney's are you?","Looks like you're going to need some roomates","I heard this city sucks anways","You can't afford to live here. Have you considered Paris? (Paris, Texas)","Bummer you can't afford to live here. Find a Delorean and go back in time to change your major."]
    var negCl = ["open","yawn","flatsquint","crying","sadeyes","frowneyes","sad"];
    var mid = ["Congrats, you can afford to live here. You just can't have any fun.","Get rid of dead weight expenses. You don't really need a cell phone do you?","Enjoy your diet of bread and water","Looks like window shopping is the only kind of shopping you can afford.","I hope you like top ramen","Enjoy being a hermit...you can't afford to go out."];
    var midCl = ["crossedmust","crossed","flat","tilt"];
    var comfort = "Congrats, you can afford to live here.";
    var comCl = ["smile","cool","money","hearts","tongue","smilewink","bigsmileeyes","bigsmile","smug","smilesquint"];
    var results = ["No, you can't afford it", "Yes, you can afford it. Barely.", "Yes, you can afford it."];

    $scope.share = function(){
        $('.controls .share').toggleClass('open');
    }
    $scope.socialShare = function(type){
        var u = location.href;
        var t = "With a degree in " + $scope.selectMajorObj[0].major + " from " + $scope.selectSchoolObj[0].school + " living in " + $scope.selectAreaObj[0].area + " I would have $" + ($scope.totalLeft.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " extra each month.";
        if(type === 'facebook'){
          sharePopup('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t));
          return false;       
        }else if(type === 'twitter'){
          sharePopup('http://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t)+'&via=1p21interactive');
          return false;
        }else if(type === 'linkedin'){
            sharePopup('https://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(u)+'&summary='+encodeURIComponent(t)+'&source=1p21interactive');
            return false;
        }
        function sharePopup(url){
          var width = 600;
          var height = 400;
          var leftPosition, topPosition;
          leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
          topPosition = (window.screen.height / 2) - ((height / 2) + 50);
          var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
          window.open(url,'Social Share', windowFeatures);
        }       
    }
    function newQuote(){
        if($scope.totalLeft < 0){
            $scope.randomQuote = neg[Math.floor(Math.random() * neg.length)];
            $scope.iconClass = negCl[Math.floor(Math.random() * negCl.length)];
            $scope.result = results[0];
        }else if($scope.totalLeft > 200){
            $scope.randomQuote = comfort;
            $scope.iconClass = comCl[Math.floor(Math.random() * comCl.length)];
            $scope.result = results[2];
        }else{
            $scope.randomQuote = mid[Math.floor(Math.random() * mid.length)];
            $scope.iconClass = midCl[Math.floor(Math.random() * midCl.length)];
            $scope.result = results[1];
        }
    }
    function taxes(val){
        return $scope.selectMajorObj[0][$scope.currentSalary.val] * val;
    }
    function totalTax(){
        $scope.taxDisplay = taxes($scope.currentTaxes.state) + taxes($scope.currentTaxes.federal);
        console.log($scope.taxDisplay);
    }
    function monthlyExpenses(){
        if($scope.selectAreaObj){
            getLivingExpenses($scope.initExpenses);
        }
        
        $scope.totalMonthly = ($scope.taxDisplay / 12) + $scope.selectPayments + $scope.livingExpenses;
        console.log($scope.totalMonthly,$scope.taxDisplay / 12,$scope.selectPayments,$scope.livingExpenses);
        $scope.totalLeft = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) - $scope.totalMonthly;
    }
    function stateTaxed(state){
        Data.get('state/'+state).then(function(state){
            $scope.stateTaxes = state.data;
            for (var i = 0; i < $scope.stateTaxes.length; i++) {
                if($scope.stateTaxes[i].income < $scope.selectMajorObj[0][$scope.currentSalary.val]){
                    $scope.currentTaxes.state = (parseFloat($scope.stateTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }
            };
        });
    }
    function fedTaxed(state){
        for (var i = $scope.federalTaxes.length - 1; i >= 0; i--) {
            var sp;
            if($scope.federalTaxes[i].single.indexOf('>') < 0){
                sp = $scope.federalTaxes[i].single.split("-");
                if(parseInt(sp[0]) <= parseInt($scope.selectMajorObj[0][$scope.currentSalary.val]) && parseInt($scope.selectMajorObj[0][$scope.currentSalary.val]) <= parseInt(sp[1])){
                    $scope.currentTaxes.federal = (parseFloat($scope.federalTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }
            }else{
                sp = parseInt($scope.federalTaxes[i].single.replace('>', ''));
                if(parseInt(sp[0]) <= parseInt($scope.selectMajorObj[0][$scope.currentSalary.val])){
                    $scope.currentTaxes.federal = (parseFloat($scope.federalTaxes[i].rate) / 100);
                    totalTax();
                    break;
                }               
            }
        };
    }
    function getLivingExpenses(arr){
        $scope.livingExpenses = 0;
        if($scope.currentExpenses.active){
            for (var i = arr.length - 1; i >= 0; i--) {
                $scope.currentExpenses[arr[i]] = $scope.expenseUpdate[arr[i]];
            };
        }else{
            for (var i = arr.length - 1; i >= 0; i--) {
                $scope.currentExpenses[arr[i]] = $scope.selectAreaObj[0][arr[i]];
            };
        }
        for (var i = arr.length - 1; i >= 0; i--) {
            $scope.livingExpenses += $scope.currentExpenses[arr[i]];
        };
    }
    function loanPayment(){
        $scope.schoolCost = ($scope.selectSchoolObj[0].tuition * $scope.yearsEnrolled) - $scope.compensation;
        var P = $scope.schoolCost;
        var J = ($scope.selectRate.interest / 100) / 12;
        var N = $scope.selectRate.term * 12;

        $scope.selectPayments = P * ( J / ( 1 - Math.pow( (1 + J), -N ) ) );
    }
    function calcArea(){
        $scope.selectAreaObj[0].otherCOLI = $scope.selectAreaObj[0].other;
        $scope.selectAreaObj[0].transportCOLI = $scope.selectAreaObj[0].transport;
        $scope.selectAreaObj[0].other = parseFloat(((($scope.selectAreaObj[0].housing + $scope.selectAreaObj[0].food) * 0.256) * ($scope.selectAreaObj[0].otherCOLI / 100)).toFixed(2));
        $scope.selectAreaObj[0].transport = parseFloat(((($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) * 0.1) * ($scope.selectAreaObj[0].transportCOLI / 100)).toFixed(2));
    }
    function getOtherExp(){
        $scope.expenses.other = parseFloat(((($scope.expenses.housing + $scope.expenses.food) * 0.256) * ($scope.selectAreaObj[0].other / 100)).toFixed(2));
        $scope.expenses.transport = parseFloat(((($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) * 0.1) * ($scope.selectAreaObj[0].transport / 100)).toFixed(2));
    }   
    function getOtherExp(){
        $scope.expenses.other = parseFloat(((($scope.expenses.housing + $scope.expenses.food) * 0.256) * ($scope.selectAreaObj[0].other / 100)).toFixed(2));
    }
    function getTransportExp(){
        $scope.expenses.transport = parseFloat(((($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) * 0.1) * ($scope.selectAreaObj[0].transport / 100)).toFixed(2));
    } 
    function calculate(sa){
        $scope.selectArea = sa;

        Data.get('expense/'+sa.id).then(function(data){
            $scope.selectAreaObj = data.data;
            calcArea();
            stateTaxed($scope.selectAreaObj[0].state);
            monthlyExpenses();
            addFinished();
            newQuote();
        });

        fedTaxed();
        monthlyExpenses();        
        loanPayment();
        newQuote();
    }
    function rotateFaces(){
        var opts = d3.selectAll('.smiley.svg .option');
        var num = Math.floor(opts.length * Math.random());
        var sel = d3.select(opts[ num ]);

        sel.classed('poop', true);
        opts.classed('dnone', false);
    }
    function addFinished(){
        var finished = {};
        finished.major = $scope.selectMajor.id;
        finished.area = $scope.selectArea.id;
        finished.school = $scope.selectSchool.id;
        Data.post('finished', finished).then(function (result) {
            if(result.status != 'error'){
                Data.get('finishone').then(function(data){
                    $scope.finishID = data.data[0]['MAX(id)'];
                    addCalculated();
                });
            }else{
                alert(result.message);
            }
        });        
    }
    function addCalculated(){
        var finished = {};
        finished.finish = $scope.finishID;
        finished.major = $scope.selectMajorObj[0].id;
        finished.area = $scope.selectAreaObj[0].id;
        finished.school = $scope.selectSchoolObj[0].id;
        finished.salary = $scope.currentSalary.val;
        finished.yrschool = $scope.yearsEnrolled;
        finished.interest = $scope.selectRate.interest;
        finished.payback = $scope.selectRate.term;
        finished.housing = $scope.currentExpenses.housing;
        finished.food = $scope.currentExpenses.food;
        finished.health = $scope.currentExpenses.premium;
        finished.transport = $scope.currentExpenses.transport;
        finished.childcare = $scope.currentExpenses.child;
        finished.other = $scope.currentExpenses.other;
        finished.state = $scope.currentTaxes.state;
        finished.fed = $scope.currentTaxes.federal;
        finished.compensation = $scope.compensation;
        Data.post('calculated', finished).then(function (result) {
            if(result.status != 'error'){
                $scope.urlCopy = $scope.homeUrl + '#/saved/' + result.lastID;
            }else{
                alert(result.message);
            }
        });        
    }


    $scope.close = function(){
        $('.edit.form').addClass('closed');
    }
    $scope.open = function(){
        $('.edit.form').removeClass('closed');
    }
    $scope.saveComplete = function(sch,lv,mj,yrs,ir,pb,cs,comp){
        $scope.close();
        if(cs){
            $scope.currentSalary = cs;
            fedTaxed();
        }
        if(yrs){
            $scope.yearsEnrolled = yrs;
            loanPayment();
        }
        if(ir){
            $scope.selectRate.interest = ir;
            loanPayment();
        }
        if(pb){
            $scope.selectRate.term = pb;
            loanPayment();
        }
        if(comp){
            $scope.compensation = comp;
            loanPayment();
        }        
        monthlyExpenses();
        
        if(sch){
            Data.get('school/'+sch.id).then(function(data){
                $scope.selectSchoolObj = data.data;
                $scope.totalMonthly = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) * 0.9;
                $scope.totalLeft = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) - $scope.totalMonthly;

                loanPayment();
                monthlyExpenses();
            });             
        }
        if(lv){
            Data.get('expense/'+lv.id).then(function(data){
                $scope.selectAreaObj = data.data;
                calcArea();
                stateTaxed($scope.selectAreaObj[0].state);
                monthlyExpenses();
            });
        }
        if(mj){
            Data.get('major/'+mj.id).then(function(data){
                $scope.selectMajorObj = data.data;
                fedTaxed();
                monthlyExpenses();
            });
        }
        setTimeout(function(){
            addCalculated();
            newQuote();
        }, 1000);
    }
    $scope.closeOverlay = function(){
        $('.living.payment .overlay').addClass('closed');
    }
    $scope.openOverlay = function(){
        $('.living.payment .overlay').removeClass('closed');
        $scope.expenses = angular.copy($scope.currentExpenses);
    }
    $scope.updateExpense = function(exp){
        $('.living.payment .overlay').addClass('closed');
        if(exp){
            $scope.expenseUpdate = exp;
            $scope.currentExpenses.active = true;
            $scope.initExpenses = [];
            for (var i = Object.keys(exp).length - 1; i >= 0; i--) {
                if(exp[Object.keys(exp)[i]] > 0){
                    $scope.initExpenses.push(Object.keys(exp)[i]);
                }
            };
            monthlyExpenses();
            setTimeout(function(){
                addCalculated();
                newQuote();
            }, 1000);
        }
    }
    $scope.goMajors = function(ss){
        if(ss){
            $scope.selectSchool = ss;
            
            Data.get('school/'+ss.id).then(function(data){
                $scope.selectSchoolObj = data.data;
            });

            if($state.current.name === 'tuition'){
                $state.go('.major');
            }else{
                $state.go('^.major');
            }
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goSchools = function(){
        if($state.current.name === 'tuition'){
            $state.go('.school');
        }else{
            $state.go('^.school');
        }
    }
    $scope.goAreas = function(sm){
        if(sm){
            $scope.selectMajor = sm;
            
            Data.get('major/'+sm.id).then(function(data){
                $scope.selectMajorObj = data.data;
            });            
            if($state.current.name === 'tuition'){
                $state.go('.area');
            }else{
                $state.go('^.area');
            }        
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.goComplete = function(sa){
        if(sa){
            calculate(sa);

            if($state.current.name === 'tuition'){
                $state.go('.complete');
            }else{
                $state.go('^.complete');
            }

            // setTimeout(function(){
            //     rotateFaces();
            // }, 1000);
        }else{
            var myEl = angular.element( document.querySelector( '.required' ) );
            myEl.removeClass('dnone');            
        }
    }
    $scope.toExpenses = function(){
        $('html,body').animate({scrollTop: $('#total-expenses').offset().top},'slow');
    }
    $scope.restart = function(){
        $state.go('^.school');
    }
    
    Data.get('schools').then(function(data){
        $scope.schools = data.data;
        $('.started').addClass('active');
        setTimeout(function(){
            $('.welcome').addClass('fadeout');
        }, 4000);
        loadload = setTimeout(function(){
            startit();
        }, 4500);
    });
    Data.get('majors').then(function(data){
        $scope.majors = data.data;
    });
    Data.get('expenses').then(function(data){
        $scope.regions = data.data;
        Data.get('expense/'+$scope.regions[Math.round(Math.random() * $scope.regions.length)].id).then(function(data){
            $scope.selectAreaObj = data.data;
        });
    });
    Data.get('federal').then(function(data){
        $scope.federalTaxes = data.data;
    });

    function startit(){
        $state.go('.school');
        $('nav.slides').removeClass('dnone');
        clearTimeout(loadload);
    }
    $scope.getStarted = function(){
        startit();
    }    
});

app.controller("completeCtrl", function ($scope, $state, Data, $stateParams, $controller) {
    $controller('ParentCtrl', {$scope: $scope});
    var neg = ["Is there room in your parent's basement?","How do you feel about permanent camping?","How attached to your kidney's are you?","Looks like you're going to need some roomates","I heard this city sucks anways","You can't afford to live here. Have you considered Paris? (Paris, Texas)","Bummer you can't afford to live here. Find a Delorean and go back in time to change your major."]
    var negCl = ["open","yawn","flatsquint","crying","sadeyes","frowneyes","sad"];
    var mid = ["Congrats, you can afford to live here. You just can't have any fun.","Get rid of dead weight expenses. You don't really need a cell phone do you?","Enjoy your diet of bread and water","Looks like window shopping is the only kind of shopping you can afford.","I hope you like top ramen","Enjoy being a hermit...you can't afford to go out."];
    var midCl = ["crossedmust","crossed","flat","tilt"];
    var comfort = "Congrats, you can afford to live here.";
    var comCl = ["smile","cool","money","hearts","tongue","smilewink","bigsmileeyes","bigsmile","smug","smilesquint"];
    var results = ["No, you can't afford it", "Yes, you can afford it. Barely.", "Yes, you can afford it."];
    var poo = 0;

    $scope.idd = $stateParams.detailId;
    $scope.urlCopy = $scope.homeUrl + '#/saved/' + $scope.idd;
    $scope.letsGo, $scope.totalLeft;
    $scope.share = function(){
        $('.controls .share').toggleClass('open');
    }
    $scope.socialShare = function(type){
        var u = location.href;
        var t = "Check out my custom college plan calculation.";
        // var t = "With a degree in " + $scope.selectMajorObj[0].major + " from " + $scope.selectSchoolObj[0].school + " living in " + $scope.selectAreaObj[0].area + " I would have $" + ($scope.totalLeft.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " extra each month.";
        if(type === 'facebook'){
          sharePopup('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t));
          return false;       
        }else if(type === 'twitter'){
          sharePopup('http://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t)+'&via=1p21interactive');
          return false;
        }else if(type === 'linkedin'){
            sharePopup('https://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(u)+'&summary='+encodeURIComponent(t)+'&source=1p21interactive');
            return false;
        }
        function sharePopup(url){
          var width = 600;
          var height = 400;
          var leftPosition, topPosition;
          leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
          topPosition = (window.screen.height / 2) - ((height / 2) + 50);
          var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
          window.open(url,'Social Share', windowFeatures);
        }       
    }
    $scope.restart = function(){
        $state.go('tuition');
    }
    $scope.toExpenses = function(){
        $('html,body').animate({scrollTop: $('#total-expenses').offset().top},'slow');
    }
    Data.get('calculated/'+ $scope.idd).then(function(data){
        console.log(data.data);

        $scope.letsGo = data.data[0];
        $scope.currentTaxes = {'state':0,'federal':0};
        $scope.initExpenses = ['food','housing','transport','premium','other'];
        $scope.livingExpenses = parseFloat($scope.letsGo.childcare) + 
                                parseFloat($scope.letsGo.food) + 
                                parseFloat($scope.letsGo.health) + 
                                parseFloat($scope.letsGo.housing) + 
                                parseFloat($scope.letsGo.other) + 
                                parseFloat($scope.letsGo.transport);
        $scope.selectRate = {'interest': $scope.letsGo.interest,'term': $scope.letsGo.payback};
        $scope.currentSalary = {};
        $scope.compensation = $scope.letsGo.compensation;
        $scope.currentSalary.val = $scope.letsGo.salary;
        switch ($scope.letsGo.salary) {
          case "mid_sal":
            $scope.currentSalary.title = 'Average Starting';
          case "mid10_sal":
            $scope.currentSalary.title = 'Lower 10% Midrage';
          case "mid25_sal":
            $scope.currentSalary.title = 'Lower 25% Midrage';
          case "mid75_sal":
            $scope.currentSalary.title = 'Lower 75% Midrage';
          case "mid90_sal":
            $scope.currentSalary.title = 'Lower 90% Midrage';           
          default:
            $scope.currentSalary.title = 'Average Starting';
        }
        if( $scope.letsGo.state !== "" ){
            $scope.currentTaxes.state = $scope.letsGo.state;
        }
        if( $scope.letsGo.fed !== "" ){
            $scope.currentTaxes.federal = $scope.letsGo.fed;
        }

        function taxes(val){
            return $scope.selectMajorObj[0][$scope.currentSalary.val] * val;
        }
        function totalTax(){
            $scope.taxDisplay = taxes($scope.currentTaxes.state) + taxes($scope.currentTaxes.federal);
        }
        function totals(){

            $scope.totalMonthly = ($scope.taxDisplay / 12) + $scope.selectPayments + $scope.livingExpenses;
            console.log($scope.totalMonthly,$scope.taxDisplay,$scope.selectPayments,$scope.livingExpenses);
            $scope.totalLeft = ($scope.selectMajorObj[0][$scope.currentSalary.val] / 12) - $scope.totalMonthly;
        }
        function loanPayment(){
            var P = ($scope.selectSchoolObj[0].tuition * $scope.letsGo.yrschool) - $scope.compensation;
            var J = ($scope.selectRate.interest / 100) / 12;
            var N = $scope.selectRate.term * 12;

            $scope.selectPayments = P * ( J / ( 1 - Math.pow( (1 + J), -N ) ) );
        }
        function newQuote(){
            if($scope.totalLeft < 0){
                $scope.randomQuote = neg[Math.floor(Math.random() * neg.length)];
                $scope.iconClass = negCl[Math.floor(Math.random() * negCl.length)];
                $scope.result = results[0];
            }else if($scope.totalLeft > 200){
                $scope.randomQuote = comfort;
                $scope.iconClass = comCl[Math.floor(Math.random() * comCl.length)];
                $scope.result = results[2];
            }else{
                $scope.randomQuote = mid[Math.floor(Math.random() * mid.length)];
                $scope.iconClass = midCl[Math.floor(Math.random() * midCl.length)];
                $scope.result = results[1];
            }
        }
        function loaded(){
            if (poo === 3) {
                loanPayment();
                totalTax();
                totals();
                newQuote();
                $state.go('saved.loaded');
            };
        }
        Data.get('school/'+$scope.letsGo.school).then(function(data){
            $scope.selectSchoolObj = data.data;
            poo += 1;
            loaded();
        });
        Data.get('expense/'+$scope.letsGo.area).then(function(data){
            $scope.selectAreaObj = data.data;
            poo += 1;
            loaded();
        });
        Data.get('major/'+$scope.letsGo.major).then(function(data){
            $scope.selectMajorObj = data.data;
            poo += 1;
            loaded();
        });
    });
});

app.controller("nfCtrl", function ($scope, $state) {
    $scope.timer = 3;
    setTimeout(function(){
        $scope.timer = 2;
        console.log($scope.timer);
    }, 1000);
    setTimeout(function(){
        $scope.timer = 1;
        console.log($scope.timer);
    }, 2000);
    setTimeout(function(){
        $scope.timer = 0;
        $state.go('tuition');
    }, 3000);
});