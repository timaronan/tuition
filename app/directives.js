app.filter('money', function () {
  return function(val) {
    return '$'+(val.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
});

app.filter('monthly', function () {
  return function(val) {
    return '$'+((val/12).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
});

app.filter('coli', function () {
  return function(val) {
  	var eval = val - 100;
  	if(eval > 0){
  		return eval + "% more than the national average";
  	}else{
  		return Math.abs(eval) + "% less than the national average";
  	}
  }
});

app.filter('housing', function () {
  return function(val) {
  	var house = 769 * (val/100);
    return '$'+(house.toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
});

app.directive('equalheight', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {

			console.log("RUNNER");

			function equalheight(container){
				var currentTallest = 0,
				     currentRowStart = 0,
				     rowDivs = new Array(),
				     $el,
				     topPosition = 0;
				$(container).each(function() {

				   $el = $(this);
				   $($el).height('auto')
				   topPostion = $el.position().top;

				   if (currentRowStart != topPostion) {
				     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				       rowDivs[currentDiv].height(currentTallest);
				     }
				     rowDivs.length = 0; // empty the array
				     currentRowStart = topPostion;
				     currentTallest = $el.height();
				     rowDivs.push($el);
				   } else {
				     rowDivs.push($el);
				     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
				  }
				   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				     rowDivs[currentDiv].height(currentTallest);
				   }
				});
			}

			// equalheight('.final.salary > div');
			equalheight('.complete > .payment');
			equalheight('.payment aside');
			equalheight('.payment article');


			$(window).load(function() {
				// equalheight('.final.salary > div');
				equalheight('.complete > .payment');
				equalheight('.payment aside');
				equalheight('.payment article');
			});


			$(window).resize(function(){
			  	// equalheight('.final.salary > div');
				equalheight('.complete > .payment');
				equalheight('.payment aside');
				equalheight('.payment article');
			});
			
		}
	};
}]);