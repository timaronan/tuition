<?php
require 'Slim/Slim.php';
require_once 'db.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$db = new dbHelper();

/**
 * Database Helper Function templates
 * select(table name, where clause as associative array)
 * insert(table name, data as associative array, mandatory column names as array)
 * update(table name, column names as associative array, where clause as associative array, required columns as array)
 * delete(table name, where clause as array)
*/

//======= GETS =======//
// Home oness
$app->get('/', 'getHome');
// Areas
$app->get('/areas', 'getAreas');
$app->get('/area/:id', 'getArea');
// Areas
$app->get('/vareas', 'getVareas');
$app->get('/varea/:id', 'getVarea');
// Majors
$app->get('/majors', 'getMajors');
$app->get('/major/:id', 'getMajor');
// Schools
$app->get('/schools', 'getSchools');
$app->get('/school/:id', 'getSchool');
//federal tax
$app->get('/federal', 'getFed');
//state tax
$app->get('/state/:st', 'getState');
//premiums
$app->get('/premiums', 'getPremiums');
//expenses
$app->get('/expenses', 'getExpenses');
$app->get('/expense/:id', 'getExpense');
//get last finish
$app->get('/finishone', 'getLastFin');
//finished
$app->get('/calculated/:id', 'getCalucated');

//======= POSTS =======//
$app->post('/expenses', 'postExpenses');
$app->post('/finished', 'postFinished');
$app->post('/calculated', 'postCalculated');

//======= PUTS =======//
$app->put('/varea/update/:id', 'putVarea');
$app->put('/expense/update/:id', 'putExp');

//======= DELETES =======//
//Games
$app->delete('/games/delete/:id', 'deleteGame');

$app->run();

/*****************
 GET FUNCTIONS
*****************/
//  .../api/
function getHome() {
    echo "Welcome to ping pong!!!";
}
//  .../api/area
function getAreas() {
    global $db;
    $rows = $db->select("areas","id,area,state,overall,housing,taxes,healthcare,childcare,transport,other",array());
    echoResponse(200, $rows);
}
function getArea($id) {
    global $db;
    $rows = $db->select("areas","id,area,state,overall,housing,taxes,healthcare,childcare,transport,other",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/varea
function getVareas() {
    global $db;
    $rows = $db->select("varea","id,state,area,housing,food,child,transport,premium,other,taxes,total,annual",array());
    echoResponse(200, $rows);
}
function getVarea($id) {
    global $db;
    $rows = $db->select("varea","id,state,area,housing,food,child,transport,premium,other,taxes,total,annual",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/major
function getMajors() {
    global $db;
    $rows = $db->select("majors","id,major",array());
    echoResponse(200, $rows);
}
function getMajor($id) {
    global $db;
    $rows = $db->select("majors","id,major,start_sal,mid_sal,start_mid_perc,mid10_sal,mid25_sal,mid75_sal,mid90_sal",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/school
function getSchools() {
    global $db;
    $rows = $db->select("schools","id,school",array());
    echoResponse(200, $rows);
}
function getSchool($id) {
    global $db;
    $rows = $db->select("schools","id,school,city,students,tuition",array('id'=>$id));
    echoResponse(200, $rows);
}
//  .../api/federal
function getFed() {
    global $db;
    $rows = $db->select("federaltax","id,rate,single,joint,seperate,hoh",array());
    echoResponse(200, $rows);
}
//  .../api/federal
function getState($st) {
    global $db;
    $rows = $db->select("state","id,state,rate,income",array('state'=>$st));
    echoResponse(200, $rows);
}
//  .../api/premiums
function getPremiums() {
    global $db;
    $rows = $db->select("premiums","id,location,premium",array());
    echoResponse(200, $rows);
}
//  .../api/expenses
function getExpenses() {
    global $db;
    $rows = $db->select("expense","id,area,state,housing",array());
    echoResponse(200, $rows);
}
function getExpense($id) {
    global $db;
    $rows = $db->select("expense","id,area,state,housing,transport,child,childEST,premium,food,other",array('id'=>$id));
    echoResponse(200, $rows);
}
// .../api/finishone
function getLastFin(){
    global $db;
    $rows = $db->selectone("finished");
    echoResponse(200, $rows);
}
// .../api/calculated/:id
function getCalucated($id){
    global $db;
    $rows = $db->select("calculated","id,finish,school,major,salary,area,yrschool,interest,payback,housing,food,health,transport,childcare,other,date_added,state,fed,compensation",array('id'=>$id));
    echoResponse(200, $rows);
}
/*****************
 POST FUNCTIONS
*****************/
//  .../api/player
function postPlayer() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('name');
    global $db;
    $rows = $db->insert("players", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player successfully added.";
    echoResponse(200, $rows);
}
function postExpenses() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array();
    global $db;
    $rows = $db->insert("expense", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player successfully added.";
    echoResponse(200, $rows);
}

function postFinished() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('area','major','school');
    global $db;
    $rows = $db->insert("finished", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player successfully added.";
    echoResponse(200, $rows);
}
function postCalculated() {
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $mandatory = array('finish','school','major','salary','area','yrschool','interest','payback','housing','food','health','transport','childcare','other');
    global $db;
    $rows = $db->insert("calculated", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player successfully added.";
    echoResponse(200, $rows);
}
/*****************
 PUT FUNCTIONS
*****************/
function putPlayer($id){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("players", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player updated successfully.";
    echoResponse(200, $rows);
}
function putVarea($id){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array('premium');
    global $db;
    $rows = $db->update("varea", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player updated successfully.";
    echoResponse(200, $rows);
}
function putExp($id){ 
    $request = \Slim\Slim::getInstance()->request();
    $data = json_decode($request->getBody());
    $condition = array('id'=>$id);
    $mandatory = array();
    global $db;
    $rows = $db->update("expense", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Player updated successfully.";
    echoResponse(200, $rows);
}
/*****************
 DELETE FUNCTIONS
*****************/
function deleteGame($id) { 
    global $db;
    $rows = $db->delete("games", array('id'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Update removed successfully.";
    echoResponse(200, $rows);
}

/***********************
    Utility Functions
***********************/
function echoResponse($status_code, $response) {
    global $app;
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response,JSON_NUMERIC_CHECK);
}

?>